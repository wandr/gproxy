# Google Proxy

A Flask application serving as a proxy for Google Maps requests to-and-fro mobile applications.

[![coverage report](https://gitlab.com/wandr/gproxy/badges/master/coverage.svg)](https://gitlab.com/wandr/gproxy/commits/master)
[![pipeline status](https://gitlab.com/wandr/gproxy/badges/master/pipeline.svg)](https://gitlab.com/wandr/gproxy/commits/master)
