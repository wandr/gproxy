FROM            registry.gitlab.com/rustydb/docker/python
MAINTAINER      russell@wandr.us

# Setup the virtual env directory.
RUN             pip install virtualenv && virtualenv /env
ENV             VIRTUAL_ENV=/env PATH=/env/bin:$PATH

# Install the application.
ADD             . /app
WORKDIR         /app
RUN             LC_CTYPE="en_US.UTF-8" /env/bin/python setup.py install && \
                pip install .[server]

# Launch gproxy!
EXPOSE          8080
ENTRYPOINT      ["/env/bin/uwsgi"]
CMD             ["app.ini"]
