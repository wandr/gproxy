"""
Copyright:
2018 Wandr LLC; All Rights Reserved.

Description:
REST interface.
"""

import json

from flask import Flask
from flask import Response
from flask import request
from wandrlib.api import connection
from wandrlib.logger import Logger
from wandrlib.util import g_normalize

LOG = Logger(__name__)
app = Flask(__name__)


@app.route('/maps/api/place/<string:service>/<string:data_type>', methods=['GET'])
def place_proxy(service: str, data_type: str) -> Response:
    """
    Route for handling all place requests.
    :param service: Place service request.
    :param data_type: Type of data.
    :returns: Google API response.
    """
    response = Response(content_type='application/json')
    data = None
    if data_type != 'json':
        response.status_code = 400
    else:
        query = g_normalize(request.args)
        with connection() as api:
            if service == 'autocomplete':
                data = api.autocomplete(**query)
            elif service == 'details':
                data = api.details(**query)
    if not data:
        response.status_code = 404
    else:
        response.data = json.dumps(data)
        response.status_code = 200
    return response


@app.route('/maps/api/geocode/<string:data_type>', methods=['GET'])
def geocode_proxy(data_type: str) -> Response:
    """
    Route for handling all geocode requests.
    :param data_type: Format of data request.
    :returns: Google API response.
    """
    response = Response(content_type='application/json')
    data = None
    if data_type != 'json':
        response.status_code = 400
    else:
        query = g_normalize(request.args)
        with connection() as api:
            data = api.geocode(**query)
    if not data:
        response.status_code = 404
    else:
        response.data = json.dumps(data)
        response.status_code = 200
    return response
