"""
Copyright:
2018 Wandr LLC; All Rights Reserved.

Description:
A Flask application for routing Google requests to-and-fro Google.
"""
from setuptools import find_packages
from setuptools import setup


def readme() -> str:
    """
    Returns the README.txt file for a long description.
    :returns: Read file as a string.
    """
    with open('README.md') as readme_file:
        return str(readme_file)


setup(
    author='Russell Bunch',
    author_email='russell@wandr.us',
    dependency_links=[
        "git+https://gitlab.com/wandr/wandrlib.git#egg=wandrlib",
    ],
    description='A REST proxy application for Google Maps requests.',
    extras_require={
        'ci': [
            'tox',
        ],
        'lint': [
            'flake8',
        ],
        'unit': [
            'coverage',
            'nose',
        ],
        'server': [
            'uwsgi<=2.0.17',
        ],
    },
    include_package_data=True,
    install_requires=[
        'wandrlib',
        'flask<1.1.0',
    ],
    long_description=readme(),
    maintainer='Wandr Team',
    maintainer_email='russell@wandr.us',
    name='gproxy',
    packages=find_packages(),
    test_suite='nose.collector',
    url='https://gitlab.com/wandr/gproxy',
    version='1.0.0',
    zip_safe=False,
)
